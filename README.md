# Overview 

This docker compose stack enable testing of elasticsearch and kibana running in separate containers. It also have a redis and logstash instance running where the logstash instance reads log data from the redis list `filebeat` in db 0 on port 6379 or 16379 when accessed from the outside of the docker compose stack.


> If you enounter that elasticsearch will not start due to permissions please update the permission in the elasticsearch directory, like:
> `sudo chmod 777 elasticsearch`
> `chmod go-w apm/config/apm-server.yml`

# Sending log data to elasticsearch

There are two ways to send log data to elasticsearch, directly or over redis. In the `filebeat/example` directory there are some examples of filebeat configuration files use the direct way or the buffered redis way.
Both the configurations expect that filebeat is running in the native space and not part of the docker compose stack.


## Redis buffer
Using the redis as an intermediate buffer the data should be written to the list called `filebeat`. This is the list that logstash is configured to `pop` log messages from.

# Get started
Start

	docker-compose  up -d


Stop 

	docker-compose  stop


Check logs

	docker-compose  logs


